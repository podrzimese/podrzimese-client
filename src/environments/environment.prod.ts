export const environment = {
  production: true,
  apiUrl: 'https://api.podrzime.se/',
  statsUrl: 'https://api.apify.com/v2/key-value-stores/K373S4uCFR9W1K8ei/records/LATEST?disableRedirect=true',
};
