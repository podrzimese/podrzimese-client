import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'collapseItems',
})
export class CollapseItemsPipe implements PipeTransform {
  transform(items: any[], shown: number, ignore: boolean) {
    if (ignore) {
      return items;
    } else {
      return items.filter((item: any, index: number) => index < shown);
    }
  }
}
