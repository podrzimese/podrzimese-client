import { Component, OnInit } from '@angular/core';
import { ResourceService } from '../resource.service';
import { About } from '@app-types/types';
import { AddSourceModalComponent } from '../add-source-modal/add-source-modal.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.sass'],
})
export class AboutComponent implements OnInit {
  content: string;

  constructor(private resourceService: ResourceService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.resourceService.getAboutContent().subscribe((response: About) => {
      this.content = response.text;
    });
  }

  public insertSource() {
    this.openDialog();
  }

  private openDialog(): void {
    const dialogRef = this.dialog.open(AddSourceModalComponent);
  }
}
