import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ResourceService } from '../resource.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NotificationService } from '../notification.service';
import { Category, CategoryType } from '@app-types/types';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import Source, { SourceType } from '@app-types/source';

const NAME_MAX_LENGTH = 100;
const DESCRIPTION_MAX_LENGTH = 350;
const LINK_MAX_LENGTH = 1500;

@Component({
  selector: 'app-add-source-modal',
  templateUrl: './add-source-modal.component.html',
  styleUrls: ['./add-source-modal.component.sass'],
})
export class AddSourceModalComponent implements OnInit {
  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(NAME_MAX_LENGTH)]),
    description: new FormControl('', [Validators.required, Validators.maxLength(DESCRIPTION_MAX_LENGTH)]),
    link: new FormControl('', [Validators.required, Validators.maxLength(LINK_MAX_LENGTH)]),
    validFrom: new FormControl(''),
    validTo: new FormControl(''),
  });
  maxLength = {
    name: NAME_MAX_LENGTH,
    description: DESCRIPTION_MAX_LENGTH,
  };
  checkedTags = false;
  checkedLocationTags = false;
  categories: any;
  roles: Category[];
  location: Category[];
  interest: Category[];
  source: Category[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private resourceService: ResourceService,
    public dialogRef: MatDialogRef<AddSourceModalComponent>,
    private notificationService: NotificationService,
    private store: Store<{ categories: { categories: Category[] } }>
  ) {}

  ngOnInit() {
    this.store.pipe(select((state) => state.categories.categories)).subscribe((categories) => {
      this.categories = categories.map((category: Category) => {
        return { id: category._id, text: category.text, type: category.type, active: false };
      });
      this.roles = this.getFilteredCategories(CategoryType.ROLE);
      this.location = this.getFilteredCategories(CategoryType.LOCATION);
      this.interest = this.getFilteredCategories(CategoryType.INTEREST);
      this.source = this.getFilteredCategories(CategoryType.SOURCE);
    });
  }

  private getFilteredCategories(categoryType: CategoryType) {
    return this.categories.filter((category: Category) => category.type === categoryType);
  }

  get nameControl() {
    return this.form.get('name');
  }

  get descriptionControl() {
    return this.form.get('description');
  }

  get linkControl() {
    return this.form.get('link');
  }

  private getActiveTags(categories: Category[]) {
    return categories.filter((category) => category.active).map((category) => category.id);
  }

  private getAllActiveTags() {
    return [
      ...this.getActiveTags(this.roles),
      ...this.getActiveTags(this.location),
      ...this.getActiveTags(this.interest),
      ...this.getActiveTags(this.source),
    ];
  }

  onSubmit() {
    if (this.form.valid) {
      const source: Source = {
        title: this.nameControl.value,
        description: this.descriptionControl.value,
        link: this.linkControl.value,
        state: SourceType.AWAITING,
        validFrom: this.form.get('validFrom').value,
        validTo: this.form.get('validTo').value,
        tags: this.getAllActiveTags(),
      };
      this.resourceService
        .createSource(source)
        .toPromise()
        .then(() => {
          this.closeModal();
          this.notificationService.showAddSourceSuccess();
        });
    } else {
      this.scrollToElement('sourceTitle');
    }
  }

  closeModal() {
    this.dialogRef.close();
  }

  private scrollToElement(elementId: string) {
    document.getElementById(elementId).scrollIntoView({ behavior: 'smooth' });
  }
}
