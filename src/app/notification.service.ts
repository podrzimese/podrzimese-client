import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor(private snackBar: MatSnackBar) {}

  showNotYetImplemented() {
    this.snackBar.open(
      'Děkujeme, že máte zájem o tuto funkcionalitu. ' +
        'Moc o vaši pomoc stojíme, ale tuto funkci ještě nemáme hotovou. Brzy to bude.',
      null,
      { duration: 3000 }
    );
  }

  showAlreadyVoted() {
    this.snackBar.open('Pro každý zdroj můžete hlasovat pouze jednou.', null, { duration: 3000 });
  }

  showReportSuccess() {
    this.snackBar.open(`Děkujeme za nahlášení problému. Jen díky vám držíme web v pořádku!`, null, { duration: 3000 });
  }

  showAddSourceSuccess() {
    this.snackBar.open(
      `Pokud víte o dalším užitečném zdroji nebo projektu, podělte se a přidejte ho také do seznamu. Jen díky vám je Podržíme.se stále aktuální.`,
      null,
      { duration: 5000 }
    );
  }
}
