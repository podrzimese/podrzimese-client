import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countItems',
})
export class CountItemsPipe implements PipeTransform {
  transform(items: any[]): unknown {
    if (items) {
      return items.length;
    } else {
      return 0;
    }
  }
}
