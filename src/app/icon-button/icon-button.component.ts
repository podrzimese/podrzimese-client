import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core/common-behaviors/color';

@Component({
  selector: 'app-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.sass'],
})
export class IconButtonComponent implements OnInit {
  @HostBinding('style.border-radius') borderRadius = '20px';
  @Input() matIcon: string;
  @Input() iconAlign: 'start' | 'end' = 'start';
  @Input() color: ThemePalette;

  constructor() {}

  ngOnInit(): void {}
}
