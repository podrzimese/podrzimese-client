import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ResourceService } from '../resource.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Report } from '@app-types/types';
import { NotificationService } from '../notification.service';

const REPORT_DEFAULT_STATE = 'open';
@Component({
  selector: 'app-report-modal',
  templateUrl: './report-modal.component.html',
  styleUrls: ['./report-modal.component.sass'],
})
export class ReportModalComponent implements OnInit {
  reasonForm = new FormGroup({
    reason: new FormControl('', [Validators.required]),
    description: new FormControl(''),
  });

  reasons = [
    'Odkaz nefunguje, jak by měl.',
    'Odkaz neodpovídá popisku.',
    'Odkaz má špatně přiřazené štítky nebo kategorii.',
    'Je tu stejný obsah vícekrát (třeba jeden projekt pod dvěma jmény)',
    'Obsah je urážlivý, agresivní nebo jinak nevhodný.',
    'Jde o spam.',
  ];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private resourceService: ResourceService,
    public dialogRef: MatDialogRef<ReportModalComponent>,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {}

  get reasonControl() {
    return this.reasonForm.get('reason');
  }

  get descriptionControl() {
    return this.reasonForm.get('description');
  }

  onSubmit() {
    if (this.reasonForm.valid) {
      const report: Report = {
        type: this.reasonControl.value,
        content: this.descriptionControl.value,
        source: this.data.source._id,
        state: REPORT_DEFAULT_STATE,
      };
      this.resourceService
        .createReport(report)
        .toPromise()
        .then(() => {
          this.closeModal();
          this.notificationService.showReportSuccess();
        });
    }
  }

  closeModal() {
    this.dialogRef.close();
  }
}
