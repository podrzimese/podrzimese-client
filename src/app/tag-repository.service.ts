import { Injectable } from '@angular/core';
import { ResourceService } from './resource.service';
import Tag from '@app-types/tag';

interface TagMap {
  [id: string]: Tag;
}

@Injectable({
  providedIn: 'root',
})
export class TagRepositoryService {
  private tags: Tag[];
  private tagMap: TagMap;

  constructor(private resourceService: ResourceService) {}

  public async initialize() {
    this.tags = (await this.resourceService.getTags().toPromise()) as Tag[];
    this.tagMap = this.tags.reduce((map, tag) => {
      map[tag._id] = tag;
      return map;
    }, {} as TagMap);
  }

  public getTags() {
    return this.tags;
  }

  public getTag(id: string) {
    return this.tagMap[id];
  }
}
