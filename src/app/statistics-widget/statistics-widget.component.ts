import { Component, OnInit } from '@angular/core';
import { ResourceService } from '../resource.service';

type Statistics = {
  totalTested: number;
  infected: number;
  recovered: number;
  deceased: number;
};

@Component({
  selector: 'app-statistics-widget',
  templateUrl: './statistics-widget.component.html',
  styleUrls: ['./statistics-widget.component.sass'],
})
export class StatisticsWidgetComponent implements OnInit {
  statistics: Statistics = {
    totalTested: -1,
    infected: -1,
    recovered: -1,
    deceased: -1,
  };
  loading = true;
  constructor(private resourceService: ResourceService) {}

  ngOnInit(): void {
    this.resourceService.getStatistics().subscribe((resp: Statistics) => {
      if (resp) {
        const { totalTested, infected, recovered, deceased } = resp;
        this.statistics = { totalTested, infected, recovered, deceased };
      }
      this.loading = false;
    });
  }
}
