import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-input-error',
  templateUrl: './input-error.component.html',
  styleUrls: ['./input-error.component.sass'],
})
export class InputErrorComponent implements OnInit {
  @Input()
  formCtrlName: FormControl;

  @Input()
  errorMessage: string;

  @Input()
  validator: string;

  constructor() {}

  ngOnInit(): void {}

  showError() {
    return this.formCtrlName.invalid && (this.formCtrlName.dirty || this.formCtrlName.touched);
  }

  showValidatorError() {
    return this.formCtrlName.errors[this.validator];
  }
}
