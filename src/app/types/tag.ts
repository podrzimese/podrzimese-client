import { CategoryType } from '@app-types/types';

export default interface Tag {
  _id: string;
  id: string;
  type: CategoryType;
  text: string;
}
