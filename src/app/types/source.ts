export default interface Source {
  _id?: string;
  title: string;
  description: string;
  link: string;
  votes?: string;
  validFrom: Date;
  validTo: Date;
  tags: string[];
  state: SourceType;
}

export enum SourceType {
  APPROVED = 'approved',
  AWAITING = 'awaiting',
}
