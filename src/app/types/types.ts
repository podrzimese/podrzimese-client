import Tag from '@app-types/tag';

export type Folder = {
  title: string;
  categories: Category[];
};

export interface Category extends Tag {
  active?: boolean;
  order?: number;
}

export type Report = {
  type: string;
  content?: string;
  source: string;
  state: string;
};

export enum CategoryType {
  SOURCE = 'source',
  ROLE = 'role',
  INTEREST = 'interest',
  LOCATION = 'location',
  SYSTEM = 'system',
  uncategorized = 'uncategorized',
}

export type About = {
  text: string;
};
