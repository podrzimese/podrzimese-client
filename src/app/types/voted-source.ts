import Source from '@app-types/source';

export default interface VotedSource extends Source {
  featured: boolean;
  votesLastWeek: number;
  votesToday: number;
  votesAll: number;
}
