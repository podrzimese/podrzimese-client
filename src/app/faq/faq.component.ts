import { Component, OnInit } from '@angular/core';
import { ResourceService } from '../resource.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.sass'],
})
export class FaqComponent implements OnInit {
  public faq$: Observable<string>;

  constructor(private resourceService: ResourceService) {}

  ngOnInit(): void {
    this.faq$ = this.resourceService.getFAQ().pipe(map((faq) => faq.text));
  }
}
