import { Component, OnInit } from '@angular/core';
import { Category } from '@app-types/types';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { changeCategoryState, resetAllCategoryStates } from '@app-store/categories.actions';
import { CategoriesModalComponent } from '../categories-modal/categories-modal.component';
import { MatDialog } from '@angular/material/dialog';
import Tag from '@app-types/tag';
import { Router } from '@angular/router';

@Component({
  selector: 'app-active-categories',
  templateUrl: './active-categories.component.html',
  styleUrls: ['./active-categories.component.sass'],
})
export class ActiveCategoriesComponent implements OnInit {
  activeCategories$: Observable<Category[]>;

  constructor(
    private store: Store<{ categories: { categories: Category[] } }>,
    private dialog: MatDialog,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activeCategories$ = this.getStoreFilteredCategories();
    this.activeCategories$.subscribe((categories) => {
      if (categories.length > 0) {
        const tag = categories.map((category) => category._id);
        this.router.navigate([], { queryParams: { tag } });
      } else {
        this.router.navigate([]);
      }
    });
  }

  public filterDialog() {
    const dialogRef = this.dialog.open(CategoriesModalComponent);
    dialogRef.afterClosed().subscribe(() => {});
  }

  public removeCategory(category: Tag) {
    this.store.dispatch(changeCategoryState({ categoryId: category._id }));
  }

  public removeAllCategories() {
    this.store.dispatch(resetAllCategoryStates());
  }

  private getStoreFilteredCategories(): Observable<Category[]> {
    return this.store.pipe(
      select((state) =>
        state.categories.categories.filter((category) => category.active).sort((a, b) => a.order - b.order)
      )
    );
  }
}
