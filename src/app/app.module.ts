import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CategoriesComponent } from './categories/categories.component';
import { HttpClientModule } from '@angular/common/http';
import { RecordListComponent } from './record-list/record-list.component';
import { GraphQLModule } from './graphql.module';
import { MainViewComponent } from './main-view/main-view.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import { ClaimComponent } from './claim/claim.component';
import { StatisticsWidgetComponent } from './statistics-widget/statistics-widget.component';
import { FooterComponent } from './footer/footer.component';
import { StoreModule } from '@ngrx/store';
import { categoryReducer } from './store/categories.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';

// Angular material modules
import { MatListModule } from '@angular/material/list';
import { MatSliderModule } from '@angular/material/slider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { AboutComponent } from './about/about.component';
import { SourceCardComponent } from './source-card/source-card.component';
import { TagRepositoryService } from './tag-repository.service';
import { CategoriesModalComponent } from './categories-modal/categories-modal.component';
import { ActiveCategoriesComponent } from './active-categories/active-categories.component';
import { ShortenTextPipe } from './shorten-text.pipe';
import { MatChipsModule } from '@angular/material/chips';
import { NgxLocalStorageModule } from 'ngx-localstorage';
import { MatRippleModule } from '@angular/material/core';
import { IconButtonComponent } from './icon-button/icon-button.component';
import { MarkdownModule } from 'ngx-markdown';
import { ReportModalComponent } from './report-modal/report-modal.component';
import { FaqComponent } from './faq/faq.component';
import { AddSourceModalComponent } from './add-source-modal/add-source-modal.component';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { InputErrorComponent } from './input-error/input-error.component';
import { CollapseItemsPipe } from './collapse-items.pipe';
import { CountItemsPipe } from './count-items.pipe';
import { MatTooltipModule } from '@angular/material/tooltip';

function initializeTagRepository(tagRepositoryService: TagRepositoryService) {
  return async () => {
    await tagRepositoryService.initialize();
  };
}

const MATERIAL_COMPONENTS = [
  MatListModule,
  MatSliderModule,
  MatGridListModule,
  MatFormFieldModule,
  MatCardModule,
  MatSidenavModule,
  MatTabsModule,
  MatButtonModule,
  MatToolbarModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatDialogModule,
  MatRadioModule,
  MatInputModule,
  MatDatepickerModule,
  MatSlideToggleModule,
  MatTooltipModule,
];

@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    RecordListComponent,
    MainViewComponent,
    HeaderComponent,
    ClaimComponent,
    StatisticsWidgetComponent,
    FooterComponent,
    AboutComponent,
    SourceCardComponent,
    ShortenTextPipe,
    CategoriesModalComponent,
    ActiveCategoriesComponent,
    IconButtonComponent,
    ReportModalComponent,
    FaqComponent,
    AddSourceModalComponent,
    InputErrorComponent,
    CollapseItemsPipe,
    CountItemsPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    GraphQLModule,
    FormsModule,
    ...MATERIAL_COMPONENTS,
    StoreModule.forRoot({ categories: categoryReducer }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    MatIconModule,
    MatMenuModule,
    MatChipsModule,
    NgxLocalStorageModule.forRoot(),
    MatRippleModule,
    MarkdownModule.forRoot(),
    ReactiveFormsModule,
    MatMomentDateModule,
  ],
  exports: [BrowserModule],
  entryComponents: [CategoriesModalComponent],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeTagRepository,
      multi: true,
      deps: [TagRepositoryService],
    },
    { provide: Window, useValue: window },
    { provide: MAT_DATE_LOCALE, useValue: 'cs_CZ' },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
