import { Component, OnInit } from '@angular/core';
import { Category, CategoryType } from '@app-types/types';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  changeCategoryState,
  resetAllCategoryStates,
  resetAllCategoryStatesByType,
  setCategories,
} from '@app-store/categories.actions';
import { TagRepositoryService } from '../tag-repository.service';
import { MatDialog } from '@angular/material/dialog';
import { CategoriesModalComponent } from '../categories-modal/categories-modal.component';
import { ActivatedRoute } from '@angular/router';
import { AddSourceModalComponent } from '../add-source-modal/add-source-modal.component';

const MAX_CATEGORY_SIZE = 5;

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.sass'],
})
export class CategoriesComponent implements OnInit {
  roles$: Observable<Category[]>;
  location$: Observable<Category[]>;
  interest$: Observable<Category[]>;
  source$: Observable<Category[]>;
  count$: Observable<number>;
  tags: string;
  expandedCategories = {
    role: false,
    location: false,
    interest: false,
    source: false,
  };
  maxCategorySize = MAX_CATEGORY_SIZE;

  constructor(
    private store: Store<{ categories: { categories: Category[] } }>,
    private tagRepositoryService: TagRepositoryService,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) {
    this.roles$ = this.getStoreFilteredCategories(CategoryType.ROLE);
    this.location$ = this.getStoreFilteredCategories(CategoryType.LOCATION);
    this.interest$ = this.getStoreFilteredCategories(CategoryType.INTEREST);
    this.source$ = this.getStoreFilteredCategories(CategoryType.SOURCE);
    this.count$ = this.store.pipe(
      select((state) => state.categories.categories.filter((category) => category.active).length)
    );
  }

  ngOnInit(): void {
    const tags = this.tagRepositoryService.getTags();
    this.store.dispatch(setCategories({ categories: tags }));
    this.restoreTagsFromUrl();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CategoriesModalComponent);

    dialogRef.afterClosed().subscribe(() => {});
  }

  handleCategoryClick(categoryId: string): void {
    this.store.dispatch(changeCategoryState({ categoryId }));
  }

  changeLocation(category: Category): void {
    this.store.dispatch(resetAllCategoryStatesByType({ category }));
    this.store.dispatch(changeCategoryState({ categoryId: category._id }));
  }

  resetCategories() {
    this.store.dispatch(resetAllCategoryStates());
  }

  insertSource() {
    const dialogRef = this.dialog.open(AddSourceModalComponent);
  }

  async expand(categories$: Observable<Category[]>) {
    categories$.subscribe((categories) => {
      const category = categories[0];
      Object.keys(this.expandedCategories).forEach((expandedCategory) => {
        this.expandedCategories[expandedCategory] = false;
      });
      this.expandedCategories[category.type] = true;
    });
  }

  private restoreTagsFromUrl() {
    const tagQueryParams = this.route.snapshot.queryParams.tag;
    if (tagQueryParams) {
      [].concat(tagQueryParams).forEach((categoryId) => {
        this.store.dispatch(changeCategoryState({ categoryId }));
      });
    }
  }

  private getStoreFilteredCategories(categoryType: string): Observable<Category[]> {
    return this.store.pipe(
      select((state) => state.categories.categories.filter((category) => category.type === categoryType))
    );
  }
}
