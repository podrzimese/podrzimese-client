import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ResourceService } from '../resource.service';

@Component({
  selector: 'app-claim',
  templateUrl: './claim.component.html',
  styleUrls: ['./claim.component.sass'],
})
export class ClaimComponent implements OnInit {
  claim$: Observable<string>;

  constructor(private resourceService: ResourceService) {
    this.claim$ = resourceService.getClaimContent();
  }

  ngOnInit(): void {}
}
