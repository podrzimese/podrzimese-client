import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass'],
})
export class FooterComponent implements OnInit {
  readonly COVID_LINK = 'https://covid19cz.cz/';
  readonly LINE_LINK = 'http://linka1212.cz/';
  readonly LUNG_VENTILATORS_LINK = 'https://www.corovent.cz/';
  readonly INFO_MAIL = 'napiste@podrzime.se';
  readonly PARTNERSHIP_MAIL = 'partneri@podrzime.se';

  constructor() {}

  ngOnInit(): void {}
}
