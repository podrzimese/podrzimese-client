import { createAction, props } from '@ngrx/store';
import { Category } from '@app-types/types';

export const changeCategoryState = createAction('[Categories] State change', props<{ categoryId: string }>());
export const resetAllCategoryStates = createAction('[Categories] Reset');
export const resetAllCategoryStatesByType = createAction(
  '[Categories] Reset all category states by type',
  props<{ category: Category }>()
);
export const setCategories = createAction('[Categories] Set categories', props<{ categories: Category[] }>());
export const activateOnlyOneCategoryState = createAction(
  '[Categories] Activate only one state',
  props<{ categoryId: string }>()
);
