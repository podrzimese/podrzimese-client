import { createReducer, on } from '@ngrx/store';
import {
  activateOnlyOneCategoryState,
  changeCategoryState,
  resetAllCategoryStatesByType,
  resetAllCategoryStates,
  setCategories,
} from './categories.actions';
import { Category } from '@app-types/types';

const CATEGORIES = [];

export interface CategoryState {
  categories: Category[];
}

export const initialState: CategoryState = { categories: initializeFolders(CATEGORIES) };
const counterR = createReducer(
  initialState,
  on(
    changeCategoryState,
    (state, { categoryId }) => (state = { categories: changeCategoryStateReducer(state.categories, categoryId) })
  ),
  on(resetAllCategoryStates, (state) => (state = { categories: resetCategoryStateReducer(state.categories) })),
  on(
    resetAllCategoryStatesByType,
    (state, { category }) => (state = { categories: resetAllCategoryStatesByTypeReducer(state.categories, category) })
  ),
  on(
    activateOnlyOneCategoryState,
    (state, { categoryId }) => (state = { categories: activateOnlyOneStateReducer(state.categories, categoryId) })
  ),
  on(setCategories, (state, { categories }) => (state = { ...state, categories: initializeFolders(categories) }))
);

export function categoryReducer(state, action) {
  return counterR(state, action);
}

function changeCategoryStateReducer(categories: Category[], categoryId: string): Category[] {
  const maxCategoryOrder = Math.max.apply(
    Math,
    categories.map((o) => {
      return o.order;
    })
  );
  return categories.map((category: Category) => {
    const order = category.active ? 0 : maxCategoryOrder + 1;
    return category._id === categoryId ? { ...category, active: !category.active, order } : category;
  });
}

function activateOnlyOneStateReducer(categories: Category[], categoryId): Category[] {
  const maxCategoryOrder = Math.max.apply(
    Math,
    categories.map((o) => {
      return o.order;
    })
  );
  return categories.map((category: Category) => {
    const active = category._id === categoryId;
    const order = active ? 1 : 0;
    return { ...category, active, order };
  });
}

function resetCategoryStateReducer(categories: Category[]): Category[] {
  return categories.map((category: Category) => {
    return { ...category, active: false, order: 0 };
  });
}

function resetAllCategoryStatesByTypeReducer(categories: Category[], category: Category) {
  return categories.map((item) => {
    if (item.type === category.type) {
      return { ...item, active: false, order: 0 };
    } else {
      return item;
    }
  });
}

function initializeFolders(categories: Category[]): Category[] {
  return categories.map((category: Category) => {
    return { ...category, active: false, order: 0 };
  });
}
