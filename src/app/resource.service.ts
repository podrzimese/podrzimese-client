import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@app-environment/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import VotedSource from '@app-types/voted-source';
import { Report, About } from '@app-types/types';
import FAQ from '@app-types/faq';

@Injectable({
  providedIn: 'root',
})
export class ResourceService {
  constructor(private httpClient: HttpClient) {}

  getStatistics() {
    return this.httpClient.get(environment.statsUrl);
  }

  getTags() {
    const options = {
      params: { _limit: '-1' },
    };
    const url = new URL('utilized-tags', environment.apiUrl);
    return this.httpClient.get(url.toString(), options);
  }

  findSource(params) {
    const options = {
      params,
    };
    const url = new URL('voted-sources', environment.apiUrl);
    return this.httpClient.get(url.toString(), options);
  }

  getSourceCount(params) {
    const options = {
      params,
    };
    const url = new URL('voted-sources/count', environment.apiUrl);
    return this.httpClient.get(url.toString(), options);
  }

  getSource(sourceId: string): Promise<VotedSource> {
    const url = new URL(`voted-sources/${sourceId}`, environment.apiUrl);
    return this.httpClient.get(url.toString()).toPromise() as Promise<VotedSource>;
  }

  createVote(sourceId: string) {
    const url = new URL('votes', environment.apiUrl);
    return this.httpClient.post(url.toString(), { source: sourceId }).toPromise();
  }

  public getClaimContent(): Observable<string> {
    const url = new URL('claim', environment.apiUrl);
    return this.httpClient.get<{ text: string }>(url.toString()).pipe(map((response) => response.text));
  }

  public getAboutContent(): Observable<About> {
    const url = new URL('about', environment.apiUrl);
    return this.httpClient.get<About>(url.toString());
  }

  public getFAQ(): Observable<FAQ> {
    const url = new URL('faq', environment.apiUrl);
    return this.httpClient.get<FAQ>(url.toString());
  }

  createReport(report: Report): Observable<any> {
    const url = new URL('reports', environment.apiUrl);
    return this.httpClient.post(url.toString(), report);
  }

  createSource(source: any) {
    const url = new URL('sources', environment.apiUrl);
    return this.httpClient.post(url.toString(), source);
  }
}
