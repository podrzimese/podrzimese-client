import { Pipe, PipeTransform } from '@angular/core';

const DEFAULT_CHARS_THRESHOLD = 50;

@Pipe({
  name: 'shortenText',
})
export class ShortenTextPipe implements PipeTransform {
  transform(value: string, length: number): string {
    if (!Number.isInteger(length) || length <= 3) {
      length = DEFAULT_CHARS_THRESHOLD;
    }
    return `${value.substr(0, length - 3)}...`;
  }
}
