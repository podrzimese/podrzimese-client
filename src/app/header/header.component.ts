import { Component, OnInit } from '@angular/core';
import { AddSourceModalComponent } from '../add-source-modal/add-source-modal.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-toolbar',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
})
export class HeaderComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  public insertSource() {
    this.openDialog();
  }

  private openDialog(): void {
    const dialogRef = this.dialog.open(AddSourceModalComponent);
  }
}
