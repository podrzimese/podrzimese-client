import { Component, OnInit } from '@angular/core';
import { Category, CategoryType } from '@app-types/types';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { changeCategoryState, resetAllCategoryStatesByType } from '@app-store/categories.actions';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-categories-modal',
  templateUrl: './categories-modal.component.html',
  styleUrls: ['./categories-modal.component.sass'],
})
export class CategoriesModalComponent implements OnInit {
  roles$: Observable<Category[]>;
  location$: Observable<Category[]>;
  interest$: Observable<Category[]>;
  source$: Observable<Category[]>;

  constructor(
    public dialogRef: MatDialogRef<CategoriesModalComponent>,
    private store: Store<{ categories: { categories: Category[] } }>
  ) {
    this.roles$ = this.getStoreFilteredCategories(CategoryType.ROLE);
    this.location$ = this.getStoreFilteredCategories(CategoryType.LOCATION);
    this.interest$ = this.getStoreFilteredCategories(CategoryType.INTEREST);
    this.source$ = this.getStoreFilteredCategories(CategoryType.SOURCE);
  }

  ngOnInit(): void {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  handleCategoryClick(categoryId: string): void {
    this.store.dispatch(changeCategoryState({ categoryId }));
  }

  private getStoreFilteredCategories(categoryType: string): Observable<Category[]> {
    return this.store.pipe(
      select((state) => state.categories.categories.filter((category) => category.type === categoryType))
    );
  }

  changeLocation(category: Category): void {
    this.store.dispatch(resetAllCategoryStatesByType({ category }));
    this.store.dispatch(changeCategoryState({ categoryId: category._id }));
  }
}
