import { Component, OnInit } from '@angular/core';
import { ResourceService } from '../resource.service';
import { Store, select } from '@ngrx/store';
import { Category, CategoryType } from '@app-types/types';
import VotedSource from '@app-types/voted-source';

enum STATE {
  APPROVED = 'approved',
  VERIFIED = 'verified',
}

type QueryParams = {
  state: STATE[];
  tags: string[];
  tags_category: string[];
};

type Pagination = {
  _limit?: number;
  _start?: number;
};
const PAGINATION_ALL_RECORDS = -1;
const PAGINATION_SIZE = 10;
const VALID_STATES = [STATE.APPROVED, STATE.VERIFIED];
@Component({
  selector: 'app-record-list',
  templateUrl: './record-list.component.html',
  styleUrls: ['./record-list.component.sass'],
})
export class RecordListComponent implements OnInit {
  sources: VotedSource[] = [];
  loading = true;
  error: any;
  sourceCount: number;
  paginationStart: 0;
  queryParams: QueryParams;
  pagination: Pagination;

  constructor(
    private store: Store<{ categories: { categories: Category[] } }>,
    private resourceService: ResourceService
  ) {}

  ngOnInit() {
    this.store
      .pipe(select((state) => state.categories.categories.filter((category) => category.active)))
      .subscribe((activeCategories) => {
        const categories = activeCategories.map((category) => category._id);
        this.queryParams = {
          state: VALID_STATES,
          tags: [],
          tags_category: categories,
        };
        this.paginationStart = 0;
        this.pagination = {
          _limit: PAGINATION_SIZE,
          _start: this.paginationStart,
        };

        this.findSource();
        this.getSourceCount();
      });
  }

  handlePaginationClick() {
    this.pagination._start = this.pagination._start + PAGINATION_SIZE;
    this.findSource(true);
  }

  showPaginationButton() {
    return this.pagination._start + PAGINATION_SIZE < this.sourceCount;
  }

  hasRecords() {
    return this.sources && this.sources.length;
  }

  private findSource(addSources: boolean = false) {
    this.resourceService.findSource({ ...this.queryParams, ...this.pagination }).subscribe((sources: VotedSource[]) => {
      this.sources = addSources ? [...this.sources, ...sources] : sources;
      this.loading = false;
    });
  }

  private getSourceCount() {
    this.resourceService.getSourceCount({ ...this.queryParams }).subscribe(
      (sourceCount: number) => {
        this.sourceCount = sourceCount;
      },
      () => {
        this.sourceCount = 0;
      }
    );
  }
}
