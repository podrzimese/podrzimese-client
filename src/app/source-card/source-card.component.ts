import { Component, OnInit, Input } from '@angular/core';

import Tag from '@app-types/tag';
import { TagRepositoryService } from '../tag-repository.service';
import { NotificationService } from '../notification.service';
import { ResourceService } from '../resource.service';
import { LocalStorageService } from 'ngx-localstorage';
import { ReportModalComponent } from '../report-modal/report-modal.component';
import { MatDialog } from '@angular/material/dialog';
import VotedSource from '@app-types/voted-source';
import { Store } from '@ngrx/store';
import { Category, CategoryType } from '@app-types/types';
import { activateOnlyOneCategoryState } from '@app-store/categories.actions';

const DECIMAL_RADIX = 10;
const LOCAL_STORAGE_VOTE_KEY = 'podrzime.se_vote';

@Component({
  selector: 'app-source-card',
  templateUrl: './source-card.component.html',
  styleUrls: ['./source-card.component.sass'],
})
export class SourceCardComponent implements OnInit {
  @Input() public source: VotedSource;
  public tags: Tag[];

  constructor(
    private tagRepositoryService: TagRepositoryService,
    private notificationService: NotificationService,
    private resourceService: ResourceService,
    private localStorageService: LocalStorageService,
    public dialog: MatDialog,
    private store: Store<{ categories: { categories: Category[] } }>,
    private window: Window
  ) {}

  async ngOnInit() {
    this.tags = this.source.tags
      .map((tagId) => {
        return this.tagRepositoryService.getTag(tagId);
      })
      .filter((tag) => {
        return tag && [CategoryType.SYSTEM, CategoryType.uncategorized].indexOf(tag.type) < 0;
      });
  }

  public async vote(sourceId: string) {
    const lsVotes = this.localStorageService.get(LOCAL_STORAGE_VOTE_KEY);
    if (lsVotes && lsVotes.includes(sourceId)) {
      this.notificationService.showAlreadyVoted();
    } else {
      await this.resourceService.createVote(sourceId);
      this.updateLocalStorageVote(lsVotes, sourceId);
      this.source = await this.resourceService.getSource(sourceId);
    }
  }

  public chooseTag(event: Event, tag: Tag) {
    event.preventDefault();
    this.store.dispatch(activateOnlyOneCategoryState({ categoryId: tag._id }));
    this.gotoTop();
  }

  public report(event: Event, source: VotedSource) {
    event.preventDefault();
    this.openDialog(source);
  }

  private updateLocalStorageVote(lsVotes: string | null, sourceId: string) {
    lsVotes = lsVotes ? `${lsVotes},${sourceId}` : `${sourceId}`;
    this.localStorageService.set(LOCAL_STORAGE_VOTE_KEY, lsVotes);
  }

  private openDialog(source: VotedSource): void {
    const dialogRef = this.dialog.open(ReportModalComponent, { data: { source } });
  }

  private gotoTop() {
    this.window.scroll(0, 0);
  }
}
