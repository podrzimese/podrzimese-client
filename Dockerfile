FROM node:13.3.0 AS compile-image
WORKDIR /opt/ng
COPY package.json package-lock.json ./
RUN npm install
ENV PATH="./node_modules/.bin:$PATH"
COPY . ./
